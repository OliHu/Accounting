package com.java3.myjavanotes.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Defines how a note gets tested
 *
 * @author Cyrill Näf
 * @see com.java3.myjavanotes.models.Note
 */
class NoteTest {
    /*
    Note test Properties
     */
    private final int id = 10;
    private final LocalDate date = LocalDate.of(2020, 1, 8);
    private final String title = "My Title";
    private final String noteText = "OMG, my note";
    /*
    Empty note holder
     */
    private Note note = null;

    @BeforeEach
    void setUp() {
        note = new Note(id, date, title, noteText);
    }

    @Test
    void isNull() {
        note = new Note();
        assertTrue(note.isNull());
        note = new Note(date, "Title", "Note");
        assertFalse(note.isNull());
    }

    @Test
    void getId() {
        assertEquals(id, note.getId());
    }

    @Test
    void getDate() {
        assertEquals(date, note.getDate());
    }

    @Test
    void setDate() {
        LocalDate newDate = LocalDate.of(2020, 2, 8);
        note.setDate(newDate);
        assertEquals(newDate, note.getDate());
    }

    @Test
    void getTitle() {
        assertEquals(title, note.getTitle());
    }

    @Test
    void setTitle() {
        String newTitle = "My new Title";
        note.setTitle(newTitle);
        assertEquals(newTitle, note.getTitle());
    }

    @Test
    void getNote() {
        assertEquals(noteText, note.getNote());
    }

    @Test
    void setNote() {
        String newNoteText = "Hmm, is this new?";
        note.setNote(newNoteText);
        assertEquals(newNoteText, note.getNote());
    }
}