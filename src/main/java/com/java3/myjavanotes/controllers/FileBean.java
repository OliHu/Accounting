package com.java3.myjavanotes.controllers;

import com.java3.myjavanotes.models.JDBCNote;
import com.java3.myjavanotes.models.Note;
import com.java3.myjavanotes.models.NoteDatastore;
import jakarta.faces.context.FacesContext;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jakarta.servlet.http.Part;
import org.w3c.dom.*;

import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.List;

/**
 * Responsible for Exporting/Importing notes via XML. Implements the Note Datastore interface
 *
 * @author Cyrill Näf
 * @see com.java3.myjavanotes.models.Note
 * @see com.java3.myjavanotes.models.JDBCNote
 */
public class FileBean {
    private NoteDatastore db = new JDBCNote();
    private Part uploadedFile;

    public Part getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(Part uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    /**
     * Downloads a xml file containing all notes from the database
     */
    public void download() {
        String xml = this.generateXML();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment;filename=file.xml");
        try {
            ServletOutputStream out = response.getOutputStream();
            out.write(xml.getBytes());
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Processes the uploaded xml file
     */
    public void upload() {
        try {
            this.parseXML();
        } catch (Exception e) {
            // We return a error if the xml import throws any exception
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.setStatus(500);
        }
    }

    /**
     * Parses the xml file
     *
     * @throws Exception Return any exception back to the caller. Caller should handle together with frontend
     */
    protected void parseXML() throws Exception {
        // XML Helpers....
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        InputStream inputStream = getUploadedFile().getInputStream();
        Document document = documentBuilder.parse(inputStream);
        // Document document = documentBuilder.parse(getUploadedFile().getInputStream());

        // Get root element
        document.getDocumentElement().normalize();

        // Get all childs of type note
        NodeList nodeList = document.getElementsByTagName("note");

        // Iterate over our childs
        for (int counter = 0; counter < nodeList.getLength(); counter++) {
            Node nNode = nodeList.item(counter);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Integer id = null;
                try {
                    id = Integer.parseInt(eElement.getAttribute("id"));
                } catch (NumberFormatException e) {
                    id = null;
                }
                String title = eElement.getElementsByTagName("title").item(0).getTextContent();
                LocalDate date = LocalDate.parse(eElement.getElementsByTagName("date").item(0).getTextContent());
                String note = eElement.getElementsByTagName("text").item(0).getTextContent();
                // Generate Note object from xml
                Note newNote = new Note(id, date, title, note);
                // Do update logic
                // We do not care about already existing elements
                if (id == null || db.getNote(newNote.getId()).isNull()) {
                    db.createNote(newNote);
                } else {
                    // Here we assume that the object with that ID already exists, so we update it
                    db.updateNote(newNote);
                }
            }
        }

    }

    /**
     * Magic, generates the xml as string
     *
     * @return XML String
     */
    protected String generateXML() {
        try {
            // Get all notes
            List<Note> notes = db.getNotes();
            // XML Helpers....
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // root element - notes
            Element root = document.createElement("notes");
            document.appendChild(root);

            // add all note's
            for (Note note : notes) {
                // note element
                Element element = document.createElement("note");
                root.appendChild(element);

                // set an attribute to note element
                Attr attr = document.createAttribute("id");
                attr.setValue(Integer.toString(note.getId()));
                element.setAttributeNode(attr);


                // title element
                Element title = document.createElement("title");
                title.appendChild(document.createTextNode(note.getTitle()));
                element.appendChild(title);

                // date element
                Element date = document.createElement("date");
                date.appendChild(document.createTextNode(note.getDate().toString()));
                element.appendChild(date);

                // note element
                Element noteE = document.createElement("text");
                noteE.appendChild(document.createTextNode(note.getNote()));
                element.appendChild(noteE);
            }
            // Transform Document to XML String
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));

            // If you use
            // StreamResult result = new StreamResult(System.out);
            // the output will be pushed to the standard output ...
            // You can use that for debugging
            return writer.toString();

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
        return "";
    }
}
