package com.java3.myjavanotes.controllers;

import com.java3.myjavanotes.models.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

/**
 * <h1>Note Bean</h1>
 *
 * <ul>Controller for the xHtml Pages detailedNote.xhtml or listOfNotes.xhtml</ul>
 * <ul>Different Methods are called from the xhtml pages and this call contains the Logic.</ul>
 *
 * @author Oliver Huber
 * @see com.java3.myjavanotes.models.JDBCNote
 * @see com.java3.myjavanotes.models.Note
 */

public class NoteBean {
    //Initialize Objects
    String pattern = "dd.MM.yyyy";
    DateFormat df = new SimpleDateFormat(pattern);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    DateFormat dfcurrent;
    Note note = new Note();
    JDBCNote jdbcNote = new JDBCNote();

    //Initialize Variables
    private Long noteId;
    private String siteTitle;
    private String backButton;
    private LocalDate date;
    private String dateText;
    private String title;
    private String Notiz;
    boolean onlyView;

    /**
     * Creates or Updates the Note after user pressed the button
     *
     * @return ListOfNotes.xml as a String
     * @throws ParseException If Date pattern is wrong
     */
    public String save() throws ParseException {
        //Fill the Note Object
        note.setNote(Notiz);
        note.setTitle(title);
        date = LocalDate.parse(dateText, formatter);
        note.setDate(date);
        //Sending Note Object to the Correct Method
        if (siteTitle.contains("erfassen")) {
            jdbcNote.createNote(note);
        } else {
            jdbcNote.updateNote(note);
        }
        //Redirecting back to the Home Screen
        return "listOfNotes.xhtml?faces-redirect=true";
    }

    /**
     * Setup detailed Note Site for creating notes
     *
     * @return detailedNote.xhtml String
     */
    public String create() {
        //Clearing Variables for Creating a new Note
        dateText = "";
        title = "";
        Notiz = "";
        //Setting Title and Button name for the site
        siteTitle = "Notiz erfassen";
        backButton = "Create";
        onlyView = false;
        //Redirect to detailedNote.xhtml
        return "detailedNote.xhtml?faces-redirect=true";
    }

    /**
     * Deleting a selected note in the note table. Uses Row Note Id as Parameter
     *
     * @return listOfNotes.xhtml
     */
    public String delete() {
        //Deleting Note with the NoteId
        try {
            jdbcNote.deleteNote(noteId.intValue());
        } catch (NullPointerException p) {
            p.printStackTrace();
        }
        //Redirecting back to home
        return "listOfNotes.xhtml?faces-redirect=true";
    }

    /**
     * Setup detailed Note Site for editing Note
     *
     * @return detailedNote.xhtml
     */
    public String edit() {
        //Filling note with note data from DB
        fillNote();
        //Setting Title and Button name for the site
        siteTitle = "Notiz editieren";
        backButton = "Update";
        onlyView = false;
        //Redirect to detailedNote.xhtml
        return "detailedNote.xhtml?faces-redirect=true";

    }

    /**
     * Setup detailed Note Site for viewing Note
     *
     * @return detailedNote.xhtml
     */
    public String view() {
        //Filling note with note data from DB
        fillNote();
        //Setting Title and Button name for the site
        siteTitle = "Notiz anzeigen";
        onlyView = true;
        //Redirect to detailedNote.xhtml
        return "detailedNote.xhtml?faces-redirect=true";
    }

    /**
     * Filling Note Values of Bean for detailed xHtml
     */
    public void fillNote() {
        //Getting note from the database
        try {
            note = jdbcNote.getNote(noteId.intValue());
        } catch (NullPointerException p) {
            p.printStackTrace();
        }
        //Setting Variables used in xHtml Site
        dateText = note.getDate().format(formatter);
        title = note.getTitle();
        Notiz = note.getNote();
    }

    /**
     * Returning Date in specific Format for xHtml
     *
     * @return Current Date as dd.MM.yyyy HH:mm:ss
     */
    public String getCurrentDate() {
        //Setting up a date Pattern
        dfcurrent = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        return dfcurrent.format(new Date());
    }
    /*
    GETTER AND SETTER
     */

    public void setSiteTitle(String siteTitle) {
        this.siteTitle = siteTitle;
    }
    public String getSiteTitle() {
        return siteTitle;
    }

    public String getTitle() {
        return title;
    }

    public String getDateText() {
        return dateText;
    }

    public void setDateText(String dateText) {
        this.dateText = dateText;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotiz() {
        return Notiz;
    }

    public void setNotiz(String notiz) {
        Notiz = notiz;
    }

    public boolean isOnlyView() {
        return onlyView;
    }

    public void setOnlyView(boolean onlyView) {
        this.onlyView = onlyView;
    }

    public Long getNoteId() {
        return noteId;
    }

    public void setNoteId(Long noteId) {
        this.noteId = noteId;
    }

    public String getBackButton() {
        return backButton;
    }

}
