package com.java3.myjavanotes.models;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;

/**
 * Connection to Database
 *
 * @author Cyrill Näf & Dane Singer
 */
public class JDBCNote implements NoteDatastore {
    private static final String DATA_SOURCE_NAME = "jdbc/simplePool";
    private InitialContext context = null;
    private DataSource ds = null;
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private ResultSet rs = null;

    /**
     * Initializes the database connection from context
     *
     * @throws Exception Return all exceptions to the caller. Caller should handle them
     */
    private void init() throws Exception {
        context = new InitialContext();
        ds = (DataSource) context.lookup(DATA_SOURCE_NAME);
        connection = ds.getConnection();
    }

    /**
     * Helper function to close all db related things again
     */
    private void close() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException var31) {
                var31.printStackTrace();
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException var30) {
                var30.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException var29) {
                var29.printStackTrace();
            }
        }
    }

    /**
     * Get's a specific note from the datastore
     *
     * @param id ID of the note
     * @return The note as Note object
     */
    public Note getNote(int id) {
        String sql = "SELECT * FROM simplejdbc.MyJavaNote WHERE ID = ? LIMIT 1";
        Note note = null;
        try {
            this.init();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.next();
            note = new Note(rs.getInt("ID"), rs.getObject("date", LocalDate.class), rs.getString("title"), rs.getString("note"));
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            this.close();
        }
        return note;
    }

    /**
     * Creates a new note in the datastore
     *
     * @param note The Note object
     */
    public void createNote(Note note) {
        String sql = "INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES (?, ?, ?);";
        try {
            this.init();
            stmt = connection.prepareStatement(sql);
            stmt.setObject(1, note.getDate());
            stmt.setString(2, note.getTitle());
            stmt.setString(3, note.getNote());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            this.close();
        }
    }

    /**
     * Deletes a note in the datastore
     *
     * @param note The Note object (ID field is mandatory!)
     */
    public void deleteNote(Note note) {
        this.deleteNote(note.getId());
    }

    /**
     * Deletes a note in the datastore
     *
     * @param id The id of a note
     */
    public void deleteNote(int id) {
        String sql = "DELETE FROM simplejdbc.MyJavaNote WHERE ID = ?";
        try {
            this.init();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            this.close();
        }
    }

    /**
     * Returns a List of all notes objects in the datastore
     *
     * @return List of Notes object
     */
    public List<Note> getNotes() {
        List<Note> noteList = new ArrayList<>();
        String sql = "SELECT * FROM simplejdbc.MyJavaNote";
        try {
            this.init();
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Note note = new Note(rs.getInt("ID"), rs.getObject("date", LocalDate.class), rs.getString("title"), rs.getString("note"));
                noteList.add(note);
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            this.close();
        }
        return noteList;
    }

    ;

    /**
     * Updates a note object
     *
     * @param note The Note object (ID field is mandatory!)
     * @return Returns the updated Note object
     */
    public Note updateNote(Note note) {
        String sql = "UPDATE simplejdbc.MyJavaNote SET date=?, title=?, note=? WHERE ID = ?";
        try {
            this.init();
            stmt = connection.prepareStatement(sql);
            stmt.setObject(1, note.getDate());
            stmt.setString(2, note.getTitle());
            stmt.setString(3, note.getNote());
            stmt.setInt(4, note.getId());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            this.close();
        }
        return this.getNote(note.getId());
    }

}
